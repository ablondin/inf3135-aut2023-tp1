# Solution du travail pratique 1

## Identification

Identifiez-vous, en indiquant votre prénom, votre nom et votre code permanent.
(supprimer ce paragraphe lors de la remise).

## Vérifications et état du projet

Indiquez tous les éléments qui ont été complétés dans la liste ci-bas en
insérant un `X` entre les crochets. Si un élément n'a pas été complété,
expliquez pourquoi (supprimer ce paragraphe lors de la remise).

* [ ] Le nom du dépôt GitLab est exactement `inf3135-aut2023-tp1`
* [ ] L'URL du dépôt GitLab est exactement (remplacer `utilisateur` par votre
  nom identifiant GitLab) `https://gitlab.info.uqam.ca/utilisateur/inf3135-aut2023-tp1`
* [ ] Les utilisateurs `blondin_al` et `dogny_g` ont accès au projet en mode
  *Developer*
* [ ] Le dépôt GitLab est un *fork* de [ce
  dépôt](https://gitlab.info.uqam.ca/inf3135-aut2023/inf3135-aut2023-tp1)
* [ ] Le dépôt GitLab est privé
* [ ] Le dépôt contient au moins un fichier `.gitignore`.
* [ ] Le fichier `Makefile` permet de compiler les solutions
* [ ] Les sections incomplètes de ce fichier ont été complétées.

## Solution de la question 1

À compléter.

## Solution de la question 2

À compléter.

## Solution de la question 3

À compléter.

## Solution de la question 4

À compléter.
