setup() {
    load 'bats/bats-support/load'
    load 'bats/bats-assert/load'
}

@test "fichier vide" {
    run bash -c "echo '' | ./mystery"
    assert_success
    assert_output ''
}
