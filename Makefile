bats = bats/bats-core/bin/bats

.PHONY: all test test-mystery

all: factorial mystery hw motzkin

test: test-mystery

factorial: factorial.c
	gcc -o $@ $<

mystery: mystery.c
	gcc -o $@ $<

hw: hw.c
	gcc -o $@ $<

motzkin: motzkin.c
	gcc -o $@ $<

test-mystery: mystery
	$(bats) mystery.bats
